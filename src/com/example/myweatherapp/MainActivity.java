package com.example.myweatherapp;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends Activity {
	private static final String TAG_LON = "lon";
	private static final String TAG_LAT = "lat";
	private static final String TAG_SUNRISE = "sunrise";
	private static final String TAG_SUNSET = "sunset";
	private static final String TAG_TEMP = "temp";
	private static final String TAG_PRESSURE = "pressure";
	private static final String TAG_HUMIDITY = "humidity";
	private static final String TAG_ICON = "icon";
	private static final String TAG_COD = "cod";
	String resURL;
	String url = "http://api.openweathermap.org/data/2.5/weather?q=";
	String weatherString;
	Context context;
	EditText mEditText;
	Button mButton;
	ProgressBar mProgressBar;
	ImageView mImageView;

	TextView mtTextView1, mtTextView2, mtTextView3, mtTextView4, mtTextView5,
			mtTextView6, mtTextView7;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
		mEditText = (EditText) findViewById(R.id.edit);
		mButton = (Button) findViewById(R.id.button);
		mtTextView1 = (TextView) findViewById(R.id.longitude);
		mtTextView2 = (TextView) findViewById(R.id.lattitude);
		mtTextView3 = (TextView) findViewById(R.id.sunrise);
		mtTextView4 = (TextView) findViewById(R.id.sunset);
		mtTextView5 = (TextView) findViewById(R.id.teperature);
		mtTextView6 = (TextView) findViewById(R.id.pressure);
		mtTextView7 = (TextView) findViewById(R.id.humidity);
		mImageView = (ImageView) findViewById(R.id.image1);
		mButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String fullURL = url + mEditText.getText().toString();
				new GetWeather().execute(fullURL);

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class GetWeather extends AsyncTask<String, Void, Void> {
		Bitmap bitMap;
		String longitude;
		String lattitude;
		String sunrise;
		String sunset, iconId;
		Double temp;
		int pressure;
		int code;
		String humidity;
		// Double temperature;
		boolean flag = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			mProgressBar.setVisibility(View.VISIBLE);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(String... urls) {
			InputStream is = null;
			int length = 1000;
			try {
				try {
					URL url = new URL(urls[0]);
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					conn.setReadTimeout(10000);
					conn.setConnectTimeout(15000);
					conn.setRequestMethod("GET");
					conn.setDoInput(true);
					// Starts the query
					conn.connect();
					is = conn.getInputStream();
					Reader reader = null;
					reader = new InputStreamReader(is, "UTF-8");
					char[] buffer = new char[length];
					reader.read(buffer);
					// Convert the InputStream into a string
					weatherString = new String(buffer);

				} finally {
					if (is != null) {
						is.close();
					}
				}
			} catch (IOException e) {
				flag = true;

			}

			//
			if (weatherString != null) {
				try {
					JSONObject jsonObject = new JSONObject(weatherString);
					JSONObject coord = jsonObject.getJSONObject("coord");
					JSONObject sys = jsonObject.getJSONObject("sys");
					JSONObject main = jsonObject.getJSONObject("main");
					JSONArray weather = jsonObject.getJSONArray("weather");
					JSONObject weatherObject = weather.getJSONObject(0);
					// JSONObject icon = iconObject.getJSONObject("icon");
					longitude = coord.getString(TAG_LON);
					lattitude = coord.getString(TAG_LAT);
					sunrise = sys.getString(TAG_SUNRISE);
					sunset = sys.getString(TAG_SUNSET);
					temp = main.getDouble(TAG_TEMP);
					pressure = main.getInt(TAG_PRESSURE);
					humidity = main.getString(TAG_HUMIDITY);
					iconId = weatherObject.getString(TAG_ICON);
					// code = jsonObject.getInt(TAG_COD);

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			resURL = "http://openweathermap.org/img/w/" + iconId + ".png";
			Log.i("resURL:", resURL);
			// int cod = code /100;
			// if(cod==8)
			// {
			// switch (code) {
			// case 800: resURL = "http://openweathermap.org/img/w/01d.png";
			// break;
			// case 801: resURL = "http://openweathermap.org/img/w/02d.png";
			// break;
			// case 802: resURL = "http://openweathermap.org/img/w/03d.png";
			// break;
			// case 803: resURL = "http://openweathermap.org/img/w/04d.png";
			// break;
			// case 804: resURL = "http://openweathermap.org/img/w/04d.png";
			// break;
			// }
			//
			// }
			// else
			// {
			// switch (cod) {
			// case 2: resURL = "http://openweathermap.org/img/w/11d.png";
			// break;
			// case 3: resURL = "http://openweathermap.org/img/w/09d.png";
			// break;
			// case 5: int codInFive = code/10;
			// switch (codInFive) {
			// case 50:resURL = "http://openweathermap.org/img/w/10d.png";
			// break;
			// case 51:resURL = "http://openweathermap.org/img/w/13d.png";
			// break;
			// case 52:resURL = "http://openweathermap.org/img/w/09d.png";
			// break;
			// case 53:resURL = "http://openweathermap.org/img/w/09d.png";
			// break;
			// }
			// break;
			//
			// case 6: resURL = "http://openweathermap.org/img/w/13d.png";
			// break;
			//
			// case 7: resURL = "http://openweathermap.org/img/w/50d.png";
			// break;
			// }
			// }
			try {
				bitMap = image(resURL);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (flag) {
				System.out
						.print("Unable to retrieve web page. City name may be invalid.");
			} else {

				Double temperature = temp - 273.15;
				DecimalFormat numberFormat = new DecimalFormat("#.00");
				mProgressBar.setVisibility(View.INVISIBLE);
				mtTextView1.setText("Lon:" + longitude);
				mtTextView2.setText("Lat" + lattitude);
				mtTextView3.setText("Sunrise:" + convetTime(sunrise));
				mtTextView4.setText("Sunset:" + convetTime(sunset));
				mtTextView5.setText("Temp:" + numberFormat.format(temperature)
						+ "C");
				mtTextView6.setText("Pressure:" + pressure + "hpa");
				mtTextView7.setText("Humidity" + humidity + "/");
				mImageView.setImageBitmap(bitMap);

				super.onPostExecute(result);
			}
		}

		public Bitmap image(String myurl) throws IOException {
			InputStream mInputStream = null;
			Bitmap bitmap;
			try {
				URL url = new URL(myurl);
				HttpURLConnection conn1 = (HttpURLConnection) url
						.openConnection();
				conn1.setReadTimeout(10000);
				conn1.setConnectTimeout(15000);
				conn1.setRequestMethod("GET");
				// conn1.setDoInput(true);
				// Starts the query
				conn1.connect();
				mInputStream = conn1.getInputStream();
				System.out.println("i am in image");

				bitmap = BitmapFactory.decodeStream(mInputStream);

			} finally {
				if (mInputStream != null) {
					mInputStream.close();
				}
			}

			return bitmap;
		}

		String convetTime(String time) {
			long dv = Long.valueOf(time) * 1000;
			Date df = new java.util.Date(dv);
			String vv = new SimpleDateFormat("hh:mma").format(df);
			return vv;

		}
	}
}
